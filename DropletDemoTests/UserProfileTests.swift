//
//  UserProfileTests.swift
//  DropletDemoTests
//
//  Created by Javier Alvargonzalez on 2020-05-14.
//  Copyright © 2020 ITIOX. All rights reserved.
//

import XCTest
@testable import DropletDemo
import CryptoKit

class UserProfileTests: XCTestCase {

    func testEncryption() throws {
        let symmetricKey = SymmetricKey(size: .bits256)

        let userProfile = UserProfile(avatar: "avatar",
                                      firstName: "firstName",
                                      lastName: "lastName",
                                      phoneNumber: "phoneNumber",
                                      email: "email",
                                      location: "location")
        let encrypted = try userProfile.encrypt(with: symmetricKey)
        let decryptedUserProfile = try UserProfile.decrypt(value: encrypted, with: symmetricKey)
        XCTAssertEqual(userProfile, decryptedUserProfile)
    }

    func testSymmetricKey() {
        XCTAssertNotNil(UIApplication.encryptionKey)
    }
}
