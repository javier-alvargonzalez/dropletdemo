//
//  UserProfile.swift
//  DropletDemo
//
//  Created by Javier.Alvargonzalez on 2020-05-14.
//  Copyright © 2020 ITIOX. All rights reserved.
//

import CryptoKit
import UIKit

extension UIApplication {


    /// Encryption key stored in the main `Bundle` for symmetric encryption.
    /// Preferably, it should be fetched from some BE service through secure means, but this is a demo 😜
    static var encryptionKey: SymmetricKey! {
        guard
            let base64Encoded = Bundle.main.object(forInfoDictionaryKey: "EncryptionKey") as? String,
            let data = Data(base64Encoded: base64Encoded) else {
            return nil
        }

        return SymmetricKey(data: data)
    }
}

struct UserProfile: Equatable {
    let avatar: String?
    let firstName: String?
    let lastName: String?
    let phoneNumber: String?
    let email: String?
    let location: String?
}

extension UserProfile: Codable {

    enum CodingError: Error {
        case encryptionFailed
        case decryptionFailed
    }

    /// Encrypts the `UserProfile` using AES encryption and encodes the result in Base64
    /// - Parameter key: symmetric key for AES encryption
    /// - Throws: encryption failure
    /// - Returns: base64 encrypted string
    func encrypt(with key: SymmetricKey = UIApplication.encryptionKey) throws -> String {
        let jsonData = try JSONEncoder().encode(self)
        guard let jsonString = String(data: jsonData, encoding: .ascii) else {
            throw CodingError.encryptionFailed
        }
        let input: [UInt8] = Array(jsonString.utf8)
        let sealedBox = try AES.GCM.seal(input, using: key)
        return sealedBox.combined!.base64EncodedString()
    }


    /// Decrypts the Base64 string into a `UserProfile`
    /// - Parameters:
    ///   - value: Base64 string with the encrypted data
    ///   - key: symmetric key for AES decryption
    /// - Throws: decryption failure
    /// - Returns: `UserProfile`
    static func decrypt(value: String, with key: SymmetricKey = UIApplication.encryptionKey) throws -> UserProfile {
        guard let combinedData = Data(base64Encoded: value) else {
            throw CodingError.decryptionFailed
        }
        let sealedBoxToOpen = try AES.GCM.SealedBox(combined: combinedData)
        let decryptedData = try AES.GCM.open(sealedBoxToOpen, using: key)
        guard
            let decryptedString = String(data: decryptedData, encoding: .ascii),
            let jsonData = decryptedString.data(using: .ascii) else {
            throw CodingError.decryptionFailed
        }
        return try JSONDecoder().decode(UserProfile.self, from: jsonData)
    }
}
