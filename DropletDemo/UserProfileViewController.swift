//
//  UserProfileViewController.swift
//  DropletDemo
//
//  Created by Javier Alvargonzalez on 2020-05-14.
//  Copyright © 2020 ITIOX. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

final class UserProfileViewController: UIViewController {

    private let avatarButton = UIButton()
    private let firstNameField = UITextField()
    private let lastNameField = UITextField()
    private let phoneNumberField = UITextField()
    private let emailField = UITextField()
    private let locationField = UITextField()

    private var avatar: UIImage? {
        didSet {
            self.avatarButton.setImage(self.avatar, for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "User Profile"
        self.setUpViews()
        self.setUpActions()

        // Load data from Firebase
        self.load()
    }

    private func setUpViews() {
        self.view.backgroundColor = .systemBackground

        // Avatar button
        let avatarButtonDim: CGFloat = 128
        self.avatarButton.setTitle("Avatar", for: .normal)
        self.avatarButton.backgroundColor = .systemFill
        self.avatarButton.translatesAutoresizingMaskIntoConstraints = false
        self.avatarButton.layer.cornerRadius = avatarButtonDim / 2
        self.avatarButton.clipsToBounds = true
        let avatarContainer = UIView()
        avatarContainer.addSubview(avatarButton)
        avatarContainer.translatesAutoresizingMaskIntoConstraints = false

        // First name field set up
        self.firstNameField.placeholder = "First name"

        // Last name field set up
        self.lastNameField.placeholder = "Last name"

        // Phone number field set up
        self.phoneNumberField.placeholder = "Phone number"
        self.phoneNumberField.keyboardType = .phonePad

        // Email field set up
        self.emailField.placeholder = "Email"
        self.emailField.keyboardType = .emailAddress

        // Location field set up
        self.locationField.placeholder = "Location"

        // Stack view top container
        let stackView = UIStackView(arrangedSubviews: [
            avatarContainer,
            self.firstNameField,
            self.lastNameField,
            self.phoneNumberField,
            self.emailField,
            self.locationField
        ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 32.0
        stackView.alignment = .leading
        self.view.addSubview(stackView)

        NSLayoutConstraint.activate([
            // Avatar image button constraints
            avatarContainer.heightAnchor.constraint(equalTo: avatarButton.heightAnchor),
            avatarContainer.widthAnchor.constraint(equalTo: stackView.widthAnchor),
            avatarButton.heightAnchor.constraint(equalToConstant: avatarButtonDim),
            avatarButton.widthAnchor.constraint(equalToConstant: avatarButtonDim),
            avatarButton.centerXAnchor.constraint(equalTo: avatarContainer.centerXAnchor),
            avatarButton.centerYAnchor.constraint(equalTo: avatarContainer.centerYAnchor),

            // Stack view top container constraints
            stackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 32.0),
            stackView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -16)
        ])
    }

    private func setUpActions() {

        // Disable back navigation so the user cannot go back to the sign in/up screen.
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sign out", style: .plain, target: self, action: #selector(signOut))

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(save))

        avatarButton.addTarget(self, action: #selector(pickAvatar), for: .touchUpInside)
    }

    @objc
    func signOut() {
        do {
            try Auth.auth().signOut()
        } catch {
            let alertController = UIAlertController(title: "Sign Out Error", message: error.localizedDescription, preferredStyle: .alert)
            self.present(alertController, animated: true)
        }
    }
}

// MARK: Persistance
private extension UserProfileViewController {

    func document(_ path: String) -> DocumentReference {
        return Firestore.firestore().collection("users").document(path)
    }

    func validate() -> UserProfile? {
        return UserProfile(avatar: self.avatar?.base64EncodedString(),
                           firstName: self.firstNameField.text,
                           lastName: self.lastNameField.text,
                           phoneNumber: self.phoneNumberField.text,
                           email: self.emailField.text,
                           location: self.locationField.text)
    }

    func load() {

        // We use the user UID because it's the most "stable" parameter available to identify the user.
        // So we name the user document with it.
        guard let userUID = Auth.auth().currentUser?.uid else {
            return
        }

        let activityIndicator = self.showActivityIndicator()

        self.document(userUID).getDocument { [weak self] snapshot, error in
            guard let `self` = self else { return }
            self.hideActivityIndicator(activityIndicator)

            // The user profile is encoded in a "private" parameter within the document data.
            guard
                let data = snapshot?.data(),
                let privateData = data["private"] as? String,
                let userProfile = try? UserProfile.decrypt(value: privateData) else {
                    return
            }

            if let imageBase64Encoded = userProfile.avatar {
                self.avatar = UIImage(base64Encoded: imageBase64Encoded)
            }
            self.firstNameField.text = userProfile.firstName
            self.lastNameField.text = userProfile.lastName
            self.phoneNumberField.text = userProfile.phoneNumber
            self.emailField.text = userProfile.email
            self.locationField.text = userProfile.location
        }
    }

    @objc
    func save() {
        guard
            let userUID = Auth.auth().currentUser?.uid,
            let userProfile = self.validate(),
            let encryptedData = try? userProfile.encrypt() else {
            return
        }

        let activityIndicator = self.showActivityIndicator()

        // The user profile is encoded in a "private" parameter within the document data.
        let data: [String: Any] = [
            "private": encryptedData
        ]
        self.document(userUID).setData(data) { [weak self] error in
            guard let `self` = self else { return }
            self.hideActivityIndicator(activityIndicator)
            if let error = error {
                self.presentAlert(title: "Error", message: error.localizedDescription)
            }

        }
    }
}

// MARK: ImagePicker
extension UserProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    /// Picks an image as the avatar of the user.
    /// It downscales the image if needed, because some images (i.e. pic from camera) can have a huge resolution.
    @objc
    func pickAvatar() {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            pickerController.sourceType = .camera
        } else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            pickerController.sourceType = .photoLibrary
        } else {
            pickerController.sourceType = .savedPhotosAlbum
        }

        let activityIndicator = self.showActivityIndicator()
        self.present(pickerController, animated: true) { [weak self] in
            guard let `self` = self else { return }
            self.hideActivityIndicator(activityIndicator)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        if let image = info[.editedImage] as? UIImage,
            let resizedImage = image.resizeImage() {
            self.avatar = resizedImage
        }
    }
}
