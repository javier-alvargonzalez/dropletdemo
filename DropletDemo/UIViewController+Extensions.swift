//
//  UIViewController+Extensions.swift
//  DropletDemo
//
//  Created by Javier.Alvargonzalez on 2020-05-14.
//  Copyright © 2020 ITIOX. All rights reserved.
//

import UIKit

extension UIViewController {

    func presentAlert(title: String, message: String? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .destructive))
        self.present(alertController, animated: true)
    }

    func showActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.backgroundColor = UIColor.systemFill.withAlphaComponent(0.0)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        let view: UIView! = self.navigationController?.view ?? self.view
        view.addSubview(activityIndicator)

        NSLayoutConstraint.activate([
            view.centerXAnchor.constraint(equalTo: activityIndicator.centerXAnchor),
            view.centerYAnchor.constraint(equalTo: activityIndicator.centerYAnchor),
            view.widthAnchor.constraint(equalTo: activityIndicator.widthAnchor),
            view.heightAnchor.constraint(equalTo: activityIndicator.heightAnchor)
        ])

        activityIndicator.startAnimating()

        UIView.animate(withDuration: 0.2) {
            activityIndicator.backgroundColor = UIColor.systemFill.withAlphaComponent(0.3)
        }
        return activityIndicator
    }

    func hideActivityIndicator(_ activityIndicator: UIActivityIndicatorView) {
        UIView.animate(withDuration: 0.2, animations: {
            activityIndicator.alpha = 0
        }) { _ in
            activityIndicator.removeFromSuperview()
        }
    }

    /*func addChild(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func removeFromParent() {
        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }*/
}

