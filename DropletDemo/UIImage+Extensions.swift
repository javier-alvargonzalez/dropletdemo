//
//  UIImage+Extensions.swift
//  DropletDemo
//
//  Created by Javier Alvargonzalez on 2020-05-14.
//  Copyright © 2020 ITIOX. All rights reserved.
//

import UIKit

extension UIImage {
    func resizeImage(to targetSize: CGSize = CGSize(width: 320, height: 320)) -> UIImage? {
        let size = self.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }

    func base64EncodedString() -> String? {
        return self.pngData()?.base64EncodedString()
    }

    convenience init?(base64Encoded: String) {
        guard let data = Data(base64Encoded: base64Encoded) else {
            return nil
        }

        self.init(data: data)
    }
}
