//
//  SignInViewController.swift
//  DropletDemo
//
//  Created by Javier Alvargonzalez on 2020-05-14.
//  Copyright © 2020 ITIOX. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

final class SignInViewController: UIViewController {

    private let emailField = UITextField()
    private let passwordField = UITextField()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Droplet Demo"
        self.setUpViews()

        Auth.auth().addStateDidChangeListener { [weak self] auth, user in
            guard let `self` = self else { return }

            if user != nil {
                self.navigationController?.pushViewController(UserProfileViewController(), animated: true)
            } else {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    private func setUpViews() {
        self.view.backgroundColor = .systemBackground

        let signManuallyLabel = UILabel()
        signManuallyLabel.text = "Sign in with your email and password"
        signManuallyLabel.font = .boldSystemFont(ofSize: 18)
        signManuallyLabel.adjustsFontSizeToFitWidth = true

        emailField.placeholder = "Here goes your email"
        emailField.keyboardType = .emailAddress

        passwordField.placeholder = "Here goes your password"
        passwordField.isSecureTextEntry = true

        let signInButton = UIButton()
        signInButton.setTitle("Sign In", for: .normal)
        signInButton.addTarget(self, action: #selector(signIn), for: .touchUpInside)
        signInButton.setTitleColor(.systemPink, for: .normal)

        let signUpButton = UIButton()
        signUpButton.setTitle("Sign Up", for: .normal)
        signUpButton.addTarget(self, action: #selector(signUp), for: .touchUpInside)
        signUpButton.setTitleColor(.systemGreen, for: .normal)

        let signWithOthersLabel = UILabel()
        signWithOthersLabel.text = "- or -"
        signWithOthersLabel.font = .boldSystemFont(ofSize: 18)

        let signInWithGoogleButton = GIDSignInButton()
        GIDSignIn.sharedInstance()?.presentingViewController = self

        let stackView = UIStackView(arrangedSubviews: [
            signManuallyLabel,
            emailField,
            passwordField,
            signInButton,
            signUpButton,
            signWithOthersLabel,
            signInWithGoogleButton
        ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 16.0
        stackView.alignment = .center
        self.view.addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 32.0),
            stackView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }

    @objc
    private func cancel() {
        self.dismiss(animated: true, completion: nil)
    }

    @objc
    private func signIn() {
        if let (email, password) = self.validateCredentials() {
            let activityIndicator = self.showActivityIndicator()
            Auth.auth().signIn(withEmail: email, password: password) { [weak self] (result, error) in
                guard let `self` = self else { return }
                self.hideActivityIndicator(activityIndicator)
                if let error = error {
                    self.presentAlert(title: "Error on Sign In", message: error.localizedDescription)
                }
            }
        }
    }

    @objc
    private func signUp() {
        if let (email, password) = self.validateCredentials() {
            let activityIndicator = self.showActivityIndicator()
            Auth.auth().createUser(withEmail: email, password: password) { [weak self] (result, error) in
                guard let `self` = self else { return }
                self.hideActivityIndicator(activityIndicator)
                if let error = error {
                    self.presentAlert(title: "Error on Sign Up", message: error.localizedDescription)
                }
            }
        }
    }
}

extension SignInViewController {

    static func validate(email: String) -> Bool {
        guard !email.isEmpty else {
            return false
        }
        // Simple regex to check for emails
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return predicate.evaluate(with: email)
    }

    static func validate(password: String) -> Bool {
        // Quick and dirty regex >=8 chars, >=1 upper case, >=1 lower case, >=1 digit
        // There might be edge cases like starting-ending spaces and such.
        let regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return predicate.evaluate(with: password)
    }


    /// Validates the email and password and show an error upon failure
    /// - Returns: email and password credentials
    private func validateCredentials() -> (email: String, password: String)? {
        guard let email = self.emailField.text, !email.isEmpty else {
            self.presentAlert(title: "Enter an email")
            self.emailField.becomeFirstResponder()
            return nil
        }

        guard let password = self.passwordField.text, !password.isEmpty else {
            self.presentAlert(title: "Enter an password")
            self.passwordField.becomeFirstResponder()
            return nil
        }

        guard Self.validate(email: email) else {
            self.presentAlert(title: "Enter a valid email")
            self.emailField.becomeFirstResponder()
            return nil
        }

        guard Self.validate(password: password) else {
            self.presentAlert(
                title: "Enter a valid password",
                message: "It must contain at least:\\n- 8 charaters\\n- 1 digit\\n- 1 uppercase\\n- 1 lowercase")
            self.passwordField.becomeFirstResponder()
            return nil
        }

        return (email, password)
    }
}
