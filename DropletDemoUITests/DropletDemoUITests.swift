//
//  DropletDemoUITests.swift
//  DropletDemoUITests
//
//  Created by Javier Alvargonzalez on 2020-05-14.
//  Copyright © 2020 ITIOX. All rights reserved.
//

import XCTest

class DropletDemoUITests: XCTestCase {

    func testSignInSignOut() {
        let app = XCUIApplication()
        app.launch()

        // Sign out if needed
        if app.navigationBars["User Profile"].exists {
            app.navigationBars["User Profile"].buttons["Sign out"].tap()
        }

        let emailTextField = app.textFields["Here goes your email"]
        emailTextField.tap()
        emailTextField.typeText("qwertyuiop@abc.com")

        UIPasteboard.general.string = "Xyz12345678"
        let passwordTextField = app.secureTextFields["Here goes your password"]
        passwordTextField.tap()
        passwordTextField.press(forDuration: 1.5)
        app.menuItems["Paste"].tap()

        app.staticTexts["Sign In"].tap()

        app.navigationBars["User Profile"].buttons["Sign out"].tap()
    }
}
